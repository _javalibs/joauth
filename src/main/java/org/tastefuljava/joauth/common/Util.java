package org.tastefuljava.joauth.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Util {
    private Util() {
    }

    public static String getText(URL url) throws IOException {
        HttpURLConnection cnt = (HttpURLConnection)url.openConnection();
        try {
            return getText(cnt);
        } finally {
            cnt.disconnect();
        }
    }

    public static String getText(HttpURLConnection cnt) throws IOException {
        cnt.connect();
        int stat = cnt.getResponseCode();
        if (stat < 200 || stat >= 300) {
            throw new IOException("HTTP Error " + stat + ": "
                    +cnt.getResponseMessage());
        }
        InputStream stream = cnt.getInputStream();
        try {
            return getText(stream, "UTF-8");
        } finally {
            stream.close();
        }
    }

    public static String getText(InputStream stream, String charset)
            throws IOException {
        Reader reader = new InputStreamReader(stream, charset);
        try {
            return getText(reader);
        } finally {
            reader.close();
        }
    }

    public static String getText(Reader reader) throws IOException {
        if (reader instanceof BufferedReader) {
            return getText((BufferedReader)reader);
        }
        BufferedReader in = new BufferedReader(reader);
        try {
            return getText(in);
        } finally {
            in.close();
        }
    }

    public static String getText(BufferedReader in) throws IOException {
        StringBuilder buf = new StringBuilder();
        String s = in.readLine();
        if (s != null) {
            while (true) {
                buf.append(s);
                s = in.readLine();
                if (s == null) {
                    break;
                }
                buf.append('\n');
            }
        }
        return buf.toString();
    }
}
