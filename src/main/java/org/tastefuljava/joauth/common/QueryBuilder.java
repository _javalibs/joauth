package org.tastefuljava.joauth.common;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class QueryBuilder {
	private final String encoding;
	private final StringBuilder buf = new StringBuilder();

	public QueryBuilder(String encoding) {
		this.encoding = encoding;
	}

	public boolean isEmpty() {
		return buf.length() == 0;
	}

	public void add(String name, Object value) {
		try {
			if (value != null) {
				if (buf.length() > 0) {
					buf.append('&');
				}
				buf.append(name);
				buf.append('=');
	            buf.append(URLEncoder.encode(value.toString(), encoding));
			}
        } catch (UnsupportedEncodingException e) {
        	throw new IllegalStateException("Unsupported encoding " + encoding);
        }
	}

    public String toURL(String url) {
    	if (buf.length() == 0) {
    		return url;
    	} else {
			return url + '?' + buf.toString();
    	}
	}

	@Override
    public String toString() {
		return buf.toString();
	}
}
