package org.tastefuljava.joauth.rs;

import org.tastefuljava.joauth.common.QueryBuilder;
import org.tastefuljava.joauth.common.TokenInfo;
import org.tastefuljava.joauth.common.Base64;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.tastefuljava.joauth.common.Util;

import java.io.IOException;
import java.net.URL;
import java.security.Principal;
import java.util.Enumeration;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class JOAuthFilter implements Filter {
    private static final Logger LOG = Logger.getLogger(JOAuthFilter.class);

    private static final Pattern BEARER_PATTERN = Pattern.compile(
            "^Bearer +([^ ]+) *$", Pattern.CASE_INSENSITIVE);

    private String baseUrl;

    @Override
    public void init(FilterConfig cfg) {
    	LOG.info("init filter");
        baseUrl = cfg.getInitParameter("auth-server-url");
        if (baseUrl != null && !baseUrl.endsWith("/")) {
            baseUrl += "/";
        }
        LOG.info("baseUrl=" + baseUrl);
    }

    @Override
    public void destroy() {        
    	LOG.info("destroy filter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        if (LOG.isDebugEnabled()) {
        	logRequest(req);
        }
        if (req.getUserPrincipal() != null && req.getRemoteUser() != null) {
            LOG.info("RemoteUser: " + req.getRemoteUser());
        } else {
            LOG.info("No principal in request: check the authorizer header");
            String authz = req.getHeader("Authorize");
            if (authz == null) {
                LOG.error("Not recognized as a bearer token: " + authz);
            	resp.sendError(403, "Forbidden");
            	return;
            }
            LOG.info("Authorize header: " + authz);
            Matcher matcher = BEARER_PATTERN.matcher(authz);
            if (!matcher.matches()) {
                LOG.error("Invalid bearer token");
            	resp.sendError(403, "Forbidden");
            	return;
            }
            final TokenInfo token = getTokenInfo(matcher.group(1));
            request = new HttpServletRequestWrapper(req) {
                @Override
                public String getRemoteUser() {
                    return token.getCn();
                }

                @Override
                public Principal getUserPrincipal() {
                    return new Principal() {
                        @Override
                        public String getName() {
                            return token.getCn();
                        }

                        @Override
                        public String toString() {
                            return getName();
                        }
                    };
                }
            };
        }
        chain.doFilter(request, response);
    }

    private TokenInfo getTokenInfo(String b64) throws IOException {
        String token = Base64.decodeString(b64);
        QueryBuilder buf = new QueryBuilder("UTF-8");
        buf.add("access_token", token);
        URL url = new URL(buf.toURL(baseUrl + "tokeninfo"));
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        String json = Util.getText(url);
        TokenInfo info = mapper.readValue(json, TokenInfo.class);
        if (info == null || info.getCn() == null) {
            throw new IOException("Could not get token info from "
                    + baseUrl);
        }
        return info;
    }

    private static void logRequest(HttpServletRequest request) {
        LOG.debug("PathInfo [" + request.getPathInfo() + "]");
        LOG.debug("PathTranslated [" + request.getPathTranslated() + "]");
        LOG.debug("ServletPath [" + request.getServletPath() + "]");
        LOG.debug("QueryString [" + request.getQueryString() + "]");
        LOG.debug("Remote address [" + request.getRemoteAddr() + ":"
                + request.getRemotePort() + "]");
        LOG.debug("Header fields :");
        for (Enumeration<?> enm = request.getHeaderNames();
                enm.hasMoreElements(); ) {
            String name = (String)enm.nextElement();
            for (Enumeration<?> enm2 = request.getHeaders(name);
                    enm2.hasMoreElements(); ) {
                String value = (String)enm2.nextElement();
                LOG.debug("    [" + name + "]=[" + value + "]");
            }
        }
        LOG.debug("Parameters:");
        for (Enumeration<?> enm = request.getParameterNames();
                enm.hasMoreElements(); ) {
            String name = (String)enm.nextElement();
            String values[] = request.getParameterValues(name);
            for (int i = 0; i < values.length; ++i) {
                if (name.equals("password")) {
                	LOG.debug("    [password]=********");
                } else {
                	LOG.debug("    [" + name + "]=[" + values[i] + "]");
                }
            }
        }
        LOG.debug("Attributes:");
        for (Enumeration<?> enm = request.getAttributeNames();
                enm.hasMoreElements(); ) {
            String name = (String)enm.nextElement();
            LOG.debug("    [" + name + "]=[" + request.getAttribute(name) + "]");
        }
    }
}
