package org.tastefuljava.joauth.client;

import java.io.IOException;

import org.apache.catalina.realm.GenericPrincipal;

public class Current {
	private static ThreadLocal<GenericPrincipal> CURRENT
			= new ThreadLocal<GenericPrincipal>();

	public static String getToken() throws IOException {
		GenericPrincipal principal = CURRENT.get();
		if (principal == null) {
			return null;
		}
		return JOAuthRealm.getToken(principal);
	}

	public static GenericPrincipal setCurrent(GenericPrincipal principal) {
		GenericPrincipal prev = CURRENT.get();
		CURRENT.set(principal);
		return prev;
	}
}
