package org.tastefuljava.joauth.client;

import org.tastefuljava.joauth.common.Base64;
import org.tastefuljava.joauth.common.TokenInfo;

import java.io.IOException;
import java.net.URLDecoder;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.catalina.Realm;
import org.apache.catalina.realm.GenericPrincipal;
import org.apache.catalina.realm.RealmBase;
import org.apache.log4j.Logger;

public class JOAuthRealm extends RealmBase {
    private static final Logger LOG = Logger.getLogger(JOAuthRealm.class);

    private final TokenManager manager = new TokenManager();
    {
        addLifecycleListener(manager);
    }

    public static String getToken(GenericPrincipal principal)
    		throws IOException {
    	Realm realm = principal.getRealm();
    	if (!(realm instanceof JOAuthRealm)) {
    		LOG.error("Cannot get token: wrong class of realm");
    		throw new IOException("Cannot get token: wrong class of realm");
    	}
    	TokenManager manager = ((JOAuthRealm)realm).manager;
    	String cred = Base64.decodeString(principal.getPassword());
    	int ix = cred.lastIndexOf(':');
    	if (ix < 0) {
    		LOG.error("Cannot get token: invalid credential");
    		throw new IOException("Cannot get token: invalid credential");
    	}
    	String username = URLDecoder.decode(cred.substring(0, ix), "UTF-8");
    	String credentials = URLDecoder.decode(cred.substring(ix+1), "UTF-8");
        TokenHolder holder = manager.authenticate(username, credentials);
		return holder.getToken();
    }

    public String getAccessTokenUri() {
        return manager.getAccessTokenUri();
    }

    public void setAccessTokenUri(String newValue) {
        manager.setAccessTokenUri(newValue);
    }

    public String getTokenInfoUri() {
        return manager.getTokenInfoUri();
    }

    public void setTokenInfoUri(String newValue) {
        manager.setTokenInfoUri(newValue);
    }

    public String getClientId() {
        return manager.getClientId();
    }

    public void setClientId(String newValue) {
        manager.setClientId(newValue);
    }

    public String getClientSecret() {
        return manager.getClientSecret();
    }

    public void setClientSecret(String newValue) {
        manager.setClientSecret(newValue);
    }

    public long getCleanupPeriod() {
    	return manager.getCleanupPeriod();
    }

    public void setCleanupPeriod(long period) {
    	manager.setCleanupPeriod(period);
    }

    public long getCleanupTolerance() {
        return manager.getCleanupTolerance();
    }

    public void setCleanupTolerance(long newValue) {
    	manager.setCleanupTolerance(newValue);
    }

    public long getCleanupMaxAge() {
        return manager.getCleanupMaxAge();
    }

    public void setCleanupMaxAge(long newValue) {
        manager.setCleanupMaxAge(newValue);
    }

    @Override
    protected String getName() {
        return "JOAuthRealm";
    }

    @Override
    protected String getPassword(String name) {
        return null;
    }

    @Override
    protected Principal getPrincipal(String string) {
        return null;
    }

    @Override
    public Principal authenticate(String username, String credentials) {
        try {
            // hack: the authorization code is passed as the password
            // ...and the redirect URI as the username
            TokenHolder holder = manager.authenticate(username, credentials);
            TokenInfo ti = holder.getInfo();
            if (ti == null) {
                ti = manager.getInfo(holder.getToken());
                holder.setInfo(ti);
                LOG.debug("Roles of " + ti.getCn()
                		+ ": " + Arrays.asList(ti.getMemberOf()));
            }
            List<String> roles = new ArrayList<String>();
            roles.add("AUTHENTICATED");
            if (ti.getMemberOf() != null) {
	            roles.addAll(Arrays.asList(ti.getMemberOf()));
            }
            String cred = Base64.encodeString(username + ':' + credentials); 
            return new GenericPrincipal(this, ti.getCn(), cred, roles);
        } catch (IOException ex) {
            LOG.error("Authentication error", ex);
            return null;
        }
    }
}
