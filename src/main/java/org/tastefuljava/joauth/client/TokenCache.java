package org.tastefuljava.joauth.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TokenCache {
    private final Map<CacheKey,TokenHolder> cache
            = new HashMap<CacheKey,TokenHolder>();

    public interface TokenHandler {
        public void handle(String redirectUri, String code,
                TokenHolder holder);
    }

    public TokenHolder get(String redirectUri, String code)
    		throws IOException {
        CacheKey key = new CacheKey(redirectUri, code);
        synchronized(cache) {
            TokenHolder holder = cache.get(key);
            if (holder == null) {
                return null;
            }
            return holder;
        }
    }

    public void put(String redirectUri, String code,
            TokenHolder holder) {
        CacheKey key = new CacheKey(redirectUri, code);
        synchronized(cache) {
            if (holder == null) {
                cache.remove(key);
            } else {
                cache.put(key, holder);
            }
        }
    }

    public void clear() {
        synchronized(cache) {
            cache.clear();
        }
    }

    public void cleanup(final long tolerance, final long maxAge,
    		final TokenHandler expiredHandler) {
        List<Runnable> actions = new ArrayList<Runnable>();
        synchronized(cache) {
        	List<CacheKey> toRemove = new ArrayList<CacheKey>();
            for (Map.Entry<CacheKey,TokenHolder> entry: cache.entrySet()) {
                final CacheKey key = entry.getKey();
                final TokenHolder holder = entry.getValue();
            	if (!holder.wasUsedSince(maxAge)) {
            		toRemove.add(key);
            	} else if (holder.expiresIn(tolerance)) {
                    actions.add(new Runnable() {
                        @Override
                        public void run() {
                            expiredHandler.handle(
                                    key.redirectUri, key.code, holder);
                        }
                    });
                }
            }
            for (CacheKey key: toRemove) {
            	cache.remove(key);
            }
        }
        for (Runnable action: actions) {
            action.run();
        }
    }

    private static final class CacheKey {
        private final String redirectUri;
        private final String code;

        CacheKey(String redirectUri, String code) {
            this.redirectUri = redirectUri;
            this.code = code;
        }

        @Override
        public String toString() {
            return code + "/" + redirectUri;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            } else if (obj == null) {
                return false;
            } else if (obj.getClass() != this.getClass()) {
                return false;
            } else {
                CacheKey other = (CacheKey)obj;
                return redirectUri.equals(other.redirectUri)
                        && code.equals(other.code);
            }
        }

        @Override
        public int hashCode() {
            return redirectUri.hashCode()*123 + code.hashCode();
        }
    }
}
