package org.tastefuljava.joauth.client;

import org.tastefuljava.joauth.common.TokenInfo;
import java.util.Date;

public class TokenHolder {
    private String token;
    private Date expiresAt;
    private Date lastUse;
    private String refreshToken;
    private TokenInfo info;

    public TokenHolder(TokenResponse res) {
        refresh(res);
        use();
    }

    public final synchronized void refresh(TokenResponse res) {
        token = res.access_token;
        expiresAt = new Date(new Date().getTime() + 1000L*res.expires_in);
        if (res.refresh_token != null) {
	        refreshToken = res.refresh_token;
        }
        info = null;
    }

    public synchronized String getToken() {
        return token;
    }

    public synchronized Date getExpiresAt() {
        return expiresAt;
    }

    public synchronized String getRefreshToken() {
        return refreshToken;
    }

    public synchronized TokenInfo getInfo() {
        return info;
    }

    public synchronized void setInfo(TokenInfo info) {
        this.info = info;
    }

    public void use() {
    	lastUse = new Date();
    }

    public boolean isExpired() {
        return expiresIn(0);
    }

    public boolean wasUsedSince(long delay) {
        return new Date().getTime() - delay <= lastUse.getTime();
    }

    public boolean expiresIn(long delay) {
        return new Date().getTime() + delay > expiresAt.getTime();
    }
}
