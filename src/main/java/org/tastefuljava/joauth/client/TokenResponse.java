package org.tastefuljava.joauth.client;

public class TokenResponse {
    public String access_token;
    public String token_type;
    public Long expires_in;
    public String refresh_token;
}
