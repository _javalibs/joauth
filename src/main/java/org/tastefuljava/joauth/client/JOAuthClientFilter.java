package org.tastefuljava.joauth.client;

import org.tastefuljava.joauth.common.QueryBuilder;

import java.io.IOException;
import java.security.Principal;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.realm.GenericPrincipal;
import org.apache.log4j.Logger;

public class JOAuthClientFilter implements Filter {
    private static final Logger LOG
    		= Logger.getLogger(JOAuthClientFilter.class);

    private String authorizeUri;
    private String clientId;
    private String loginPath;
    private String redirectPath;
    private String scope;
    private String errorPath;

    @Override
    public void init(FilterConfig conf) throws ServletException {
    	if (LOG.isDebugEnabled()) {
	    	LOG.debug("init, filter");
	    	@SuppressWarnings("unchecked")
            Enumeration<String> enm = conf.getInitParameterNames();
	    	while (enm.hasMoreElements()) {
	    		String name = enm.nextElement();
	    		LOG.debug("    " + name + ": " + conf.getInitParameter(name));
	    	}
    	}
        authorizeUri = conf.getInitParameter("authorize-uri");
        clientId = conf.getInitParameter("client-id");
        loginPath = conf.getInitParameter("login-path");
        errorPath = conf.getInitParameter("error-path");
        redirectPath = conf.getInitParameter("redirect-path");
        scope = conf.getInitParameter("scope");
    }

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
    	LOG.debug("doFilter");
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse resp = (HttpServletResponse)response;
        String path = getFullPath(req);
    	LOG.debug("doFilter, path=" + path);
        if (path.equals(loginPath)) {
            requestAuthCode(req, resp);
        } else if (path.equals(redirectPath)) {
        	LOG.info("OAuth redirect path");
        	String error = request.getParameter("error");
            if (error != null) {
            	LOG.error("Error, redirecting to path: " + errorPath);
                resp.sendRedirect(getRootURL(req) + errorPath);
            } else {
                handleAuthCode(req, resp);
            }
        } else {
        	GenericPrincipal prev = null;
        	Principal principal = req.getUserPrincipal();
        	if (principal instanceof GenericPrincipal) {
	        	prev = Current.setCurrent((GenericPrincipal)principal);
        	}
        	try {
	            chain.doFilter(request, response);
        	} finally {
        		Current.setCurrent(prev);
        	}
        }
    }

    private static String getRootURL(HttpServletRequest req) {
        String host = req.getHeader("X-Forwarded-Server");
        if (host == null) {
            host = req.getServerName();
        }
        String s = req.getHeader("X-Forwarded-Port");
        int port = s == null ? req.getServerPort() : Integer.parseInt(s);
        String scheme = req.getScheme();
        StringBuilder buf = new StringBuilder();
        buf.append(scheme);
        buf.append("://");
        buf.append(host);
        if (!(scheme.equals("https") && port == 443
        		|| scheme.equals("http") && port == 80)) {
            buf.append(':');
            buf.append(port);
        }
        return buf.toString();
    }

    private static String getFullPath(HttpServletRequest req) {
        StringBuilder buf = new StringBuilder();
        buf.append(req.getContextPath());
        buf.append(req.getServletPath());
        String pathInfo = req.getPathInfo();
        if (pathInfo != null) {
            buf.append(pathInfo);
        }
        return buf.toString();
    }

    private static String buildPath(String base, String name) {
        int ix = base.lastIndexOf('/');
        if (ix < 0) {
            return name;
        } else {
            return base.substring(0, ix+1) + name;
        }
    }

    private void requestAuthCode(HttpServletRequest req,
            HttpServletResponse resp) throws IOException {
        QueryBuilder buf = new QueryBuilder("UTF-8");
        buf.add("response_type", "code");
        buf.add("client_id", clientId);
        buf.add("redirect_uri", getRootURL(req) + redirectPath);
        buf.add("scope", scope);
        resp.sendRedirect(buf.toURL(authorizeUri));
    }

    private void handleAuthCode(HttpServletRequest req,
            HttpServletResponse resp) throws IOException {
    	LOG.info("Handle redirect from OAuth server");
        String path = buildPath(loginPath, "j_security_check");
        QueryBuilder buf = new QueryBuilder("UTF-8");
        buf.add("j_username", getRootURL(req) + redirectPath);
        buf.add("j_password", req.getParameter("code"));
        resp.sendRedirect(buf.toURL(getRootURL(req) + path));
    }
}
