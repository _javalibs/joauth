package org.tastefuljava.joauth.client;

import static org.apache.catalina.Lifecycle.START_EVENT;
import static org.apache.catalina.Lifecycle.STOP_EVENT;
import org.tastefuljava.joauth.client.TokenCache.TokenHandler;
import org.tastefuljava.joauth.common.Base64;
import org.tastefuljava.joauth.common.QueryBuilder;
import org.tastefuljava.joauth.common.TokenInfo;
import org.tastefuljava.joauth.common.Util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.catalina.LifecycleEvent;
import org.apache.catalina.LifecycleListener;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TokenManager implements LifecycleListener {
    private static final Logger LOG = Logger.getLogger(TokenManager.class);

    private final ObjectMapper mapper;
    private Timer timer;
    private long cleanupPeriod = 5*1000L;
    private long cleanupTolerance = 10*1000L;
    private long cleanupMaxAge = 10*60*1000L;
    private String accessTokenUri;
    private String tokenInfoUri;
    private String clientId;
    private String clientSecret;
    private final TokenCache tokenCache = new TokenCache();

    TokenManager() {
        mapper = new ObjectMapper();
        mapper.configure(
                DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public long getCleanupPeriod() {
        return cleanupPeriod;
    }

    public void setCleanupPeriod(long newValue) {
        cleanupPeriod = newValue;
    }

    public long getCleanupTolerance() {
        return cleanupTolerance;
    }

    public void setCleanupTolerance(long newValue) {
    	cleanupTolerance = newValue;
    }

    public long getCleanupMaxAge() {
        return cleanupMaxAge;
    }

    public void setCleanupMaxAge(long newValue) {
        cleanupMaxAge = newValue;
    }

    public String getAccessTokenUri() {
        return accessTokenUri;
    }

    public void setAccessTokenUri(String accessTokenUri) {
        this.accessTokenUri = accessTokenUri;
    }

    public String getTokenInfoUri() {
        return tokenInfoUri;
    }

    public void setTokenInfoUri(String tokenInfoUri) {
        this.tokenInfoUri = tokenInfoUri;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

	public TokenHolder authenticate(String redirectUri, String code)
            throws IOException {
        TokenHolder holder = tokenCache.get(redirectUri, code);
        if (holder != null) {
        	if (holder.expiresIn(cleanupTolerance)) {
        		requestRefresh(holder);
        	}
        	holder.use();
            return holder;
        }
        URL url = new URL(accessTokenUri);
        HttpURLConnection cnt = (HttpURLConnection)url.openConnection();
        try {
            cnt.addRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            cnt.setRequestMethod("POST");
            cnt.setDoOutput(true);
            QueryBuilder buf = new QueryBuilder("UTF-8");
            buf.add("grant_type", "authorization_code");
            buf.add("code", code);
            buf.add("redirect_uri", redirectUri);
            buf.add("client_id", clientId);
            buf.add("client_secret", clientSecret);
            OutputStream out = cnt.getOutputStream();
            try {
                out.write(buf.toString().getBytes("ASCII"));
            } finally {
                out.close();
            }
            String json = Util.getText(cnt);
            LOG.info(json);
            TokenResponse res = mapper.readValue(json, TokenResponse.class);
            holder = new TokenHolder(res);
            tokenCache.put(redirectUri, code, holder);
            return holder;
        } finally {
            cnt.disconnect();
        }
    }

    public TokenInfo getInfo(String token) throws IOException {
    	QueryBuilder buf = new QueryBuilder("UTF-8");
    	buf.add("access_token", token);
        URL url = new URL(buf.toURL(tokenInfoUri));
        String json = Util.getText(url);
        TokenInfo info = mapper.readValue(json, TokenInfo.class);
        if (info == null || info.getCn() == null) {
            throw new IOException("Could not get token info from "
                    + tokenInfoUri);
        }
        return info;
    }

    @Override
    public void lifecycleEvent(LifecycleEvent le) {
        if (START_EVENT.equals(le.getType())) {
            startup();
        } else if (STOP_EVENT.equals(le.getType())) {
            shutdown();
        }
    }

    private void startup() {
    	LOG.info("TokenManager startup");
    	if (cleanupPeriod != 0) {
	        timer = new Timer();
	        timer.scheduleAtFixedRate(new TimerTask() {
	            @Override
	            public void run() {
	                cleanupCache();
	            }
	        }, 0, cleanupPeriod);
    	}
    }

    private void shutdown() {
    	LOG.info("TokenManager shutdown");
        if (timer != null) {
            timer.cancel();
            timer = null;
            tokenCache.clear();
        }
    }

    private void cleanupCache() {
    	LOG.info("Cleanup cache");
        tokenCache.cleanup(cleanupTolerance, cleanupMaxAge, new TokenHandler() {
            @Override
            public void handle(String redirectUri, String code,
                    TokenHolder holder) {
                try {
                    requestRefresh(holder);
                } catch (IOException ex) {
                    LOG.error("Could not refresh token for "
                            + code, ex);
                }
            }
        });
    }

    private void requestRefresh(TokenHolder holder) throws IOException {
        URL url = new URL(accessTokenUri);
        HttpURLConnection cnt = (HttpURLConnection)url.openConnection();
        try {
            cnt.setRequestMethod("POST");
            cnt.addRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            cnt.addRequestProperty("Authorization", "Basic "
                    + Base64.encodeString(clientId + ":" + clientSecret));
            cnt.setDoOutput(true);
            QueryBuilder buf = new QueryBuilder("UTF-8");
            buf.add("grant_type", "refresh_token");
            buf.add("refresh_token", holder.getRefreshToken());
            String query = buf.toString();
            if (LOG.isDebugEnabled()) {
            	LOG.debug("Refresh token [" + accessTokenUri + "], [" + query
            			+ "] [" + clientId + ":" + clientSecret + "]");
            }
            OutputStream out = cnt.getOutputStream();
            try {
                out.write(query.getBytes("ASCII"));
            } finally {
                out.close();
            }
            String json = Util.getText(cnt);
            LOG.info(json);
            TokenResponse res = mapper.readValue(json, TokenResponse.class);
            holder.refresh(res);
        } catch (IOException e) {
        	LOG.error("Could not refresh token", e);
        	throw e;
        } finally {
            cnt.disconnect();
        }
    }
}
